Rails.application.routes.draw do
  get 'home/index'
  root 'home#index' 
  resources :reservations
  resources :books
  resources :librarians
  resources :authors
  resources :categories
  resources :clients
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
