# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'faker'

puts "Nomes dos autores"
8.times do |i|
Author.create!(
    name:Faker::Book.author
)
end
puts "Nomes dos autores OK"

puts "Nomes das Categorias"
8.times do |i|
    Category.create!(
        name:Faker::Book.genre
    )
end
puts "Nomes das Categorias OK"

puts "Nomes dos Clientes"
8.times do |i|
    Client.create!(
        name:Faker::Name.name_with_middle
    )
end
puts "Nomes dos Clientes OK"

puts "Nomes dos Bibliotecários"
8.times do |i|
    Librarian.create!(
        email:Faker::Internet.email
    )
end
puts "Nomes dos Bibliotecários OK"

puts "Nomes dos Livros"
8.times do |i|
    Book.create!(
        name:Faker::Book.title,
        author:Author.all.sample,
        category:Category.all.sample,
        stock:Faker::Number.decimal_part(digits: 2)
    )
end
puts "Nomes das Livros OK"
